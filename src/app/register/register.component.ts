import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../model/user.model';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registrationForm: FormGroup;
    user = new User('', '', '', []);
    isRegistered = false;
    submitted = false;
    errorMessage = '';

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit(): void {

    this.registrationForm = new FormGroup({
      userName: new FormControl(null, [Validators.required, Validators.minLength(5),
         Validators.maxLength(20)]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, 
        Validators.minLength(8)])
  });
  }

  onSubmit(){
    this.submitted = true;
    this.user.username = this.registrationForm.value.userName;
    this.user.email = this.registrationForm.value.email;
    this.user.password = this.registrationForm.value.password;
    this.registerUser();
  }

  createRoles(rolesList): FormArray{
    const arr = rolesList.map(role => {
    return new FormControl(role.selected)
    });
    return new FormArray(arr);
}

 registerUser(){
        this.authService.signup(this.user)
        .subscribe(user=> {
            this.isRegistered = true;
            this.router.navigate(['/login']);
        }, error=> {
            this.errorMessage = error;
            this.isRegistered = false;
        });
    }

      
    


  }


