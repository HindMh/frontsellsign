export class Recipient{
    id:number;
    civility:number;
    firstname: string;
    lastname:string;
    email:string;
    adress:string;
    zipCode: string;
    city:string;
    cellPhone: string;
    lastUpdateDate:number;
    lastUpdatePlace:number;
    localisation:string;

   
  
}