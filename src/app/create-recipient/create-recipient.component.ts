import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Recipient } from '../model/recipient.model';
import { RecipientService } from '../service/recipient.service';

@Component({
  selector: 'app-create-recipient',
  templateUrl: './create-recipient.component.html',
  styleUrls: ['./create-recipient.component.css']
})
export class CreateRecipientComponent implements OnInit {

  recipientsForm: FormGroup;
  recipient=new Recipient();
  submitted = false;
  isRegistered = false;
  errorMessage = '';
  
  constructor(
    private recipientService:RecipientService,
    private router:Router){}

  ngOnInit(): void {

    this.recipientsForm = new FormGroup({
      firstname: new FormControl(null, [Validators.required,Validators.minLength(4),
         Validators.maxLength(20)]),
      lastname: new FormControl(null, [Validators.required, Validators.minLength(5),
          Validators.maxLength(20)]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      cellPhone: new FormControl(null, [Validators.required, 
        Validators.pattern('(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})')
      ]),
      zipCode: new FormControl(null, [Validators.required,
        Validators.pattern('[0-9]{5}')]),
      city: new FormControl(null, [Validators.required,Validators.minLength(4),
         Validators.maxLength(20)]),
      adress: new FormControl(null, [Validators.required, Validators.minLength(5),
            Validators.maxLength(20)]),
            localisation: new FormControl(null, [Validators.required, Validators.minLength(5),
                Validators.maxLength(20)]),
    });
  }

 
  save() {
    
    this.recipient = this.recipientsForm.value;
    this.recipientService.createRecipient(this.recipient)
      .subscribe(data => {
          this.isRegistered=true;
      }, error =>{this.errorMessage=error});
    this.gotoListRecipient();

  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }
  
  gotoListRecipient() {
    this.recipientService.nextRefreshRecipient(true);
    this.router.navigate(['/recipients']);
  }

}