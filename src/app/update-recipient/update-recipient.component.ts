import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Recipient } from '../model/recipient.model';
import { RecipientService } from '../service/recipient.service';

@Component({
  selector: 'app-update-recipient',
  templateUrl: './update-recipient.component.html',
  styleUrls: ['./update-recipient.component.css']
})
export class UpdateRecipientComponent implements OnInit,  OnDestroy {

  public recipient: Recipient;
  public recipientSubscription: Subscription;
  recipientsForm: FormGroup;
  isRegistered: boolean = false;
  errorMessage = '';
  submitted = false;
  constructor(private router: Router, private route: ActivatedRoute,
              private recipientService : RecipientService) { }
  ngOnDestroy(): void {
    if(this.recipientSubscription)
      this.recipientSubscription.unsubscribe();
  }

  ngOnInit(): void {

    this.recipientSubscription = this.recipientService.recipientNext$
          .subscribe(item => {
            this.recipient = item;
           
          });
   
    this.initRecipientForm();
    
  }

  initRecipientForm() {

    this.recipientsForm = new FormGroup({
      firstname: new FormControl(null, [Validators.required,Validators.minLength(4),
         Validators.maxLength(20)]),
      lastname: new FormControl(null, [Validators.required, Validators.minLength(5),
          Validators.maxLength(20)]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      cellPhone: new FormControl(null, [Validators.required, 
        Validators.pattern('(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})')
      ]),
      zipCode: new FormControl(null, [Validators.required,
        Validators.pattern('[0-9]{5}')]),
      city: new FormControl(null, [Validators.required,Validators.minLength(4),
         Validators.maxLength(20)]),
      adress: new FormControl(null, [Validators.required, Validators.minLength(5),
            Validators.maxLength(20)]),
            localisation: new FormControl(null, [Validators.required, Validators.minLength(5),
                Validators.maxLength(20)]),
    });

  }

  save() {
    
    let recipientId = this.recipient.id;
    this.recipient = this.recipientsForm.value;
    this.recipientService.updateRecipient(this.recipient, recipientId)
      .subscribe(data => {
          this.isRegistered=true;
      }, error =>{this.errorMessage=error});

    this.gotoListRecipient();

  }

  onSubmit() {
    this.submitted = true;
    this.save();    
  }

  
  gotoListRecipient() {
    this.recipientService.nextRefreshRecipient(true);
    this.router.navigate(['/recipients']);
  }


}
