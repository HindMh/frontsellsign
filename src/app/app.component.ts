import { Component, OnInit } from "@angular/core";
import { AuthService } from "./service/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private authService: AuthService){}
  userName: string = '';
  ngOnInit(): void {
  }
  getUserName(){
     return sessionStorage.getItem("username");
  }
  onLogOut(){
    this.authService.logout();
  }

  loggedIn(){
    return this.authService.isLoggedIn()
  }
}