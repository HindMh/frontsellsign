import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  errorMessage = '';
  isLoggedin = false;
  isLoginFailed = false;

  constructor(private authService:AuthService,private router:Router) { }


  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
  });
  }

  onSubmit(){
    this.submitted = true;
    this.authService.login(this.loginForm.value.username, this.loginForm.value.password).subscribe(
        data=>{
            this.isLoggedin = true
            this.router.navigate(['/home']);
        },
        error=>{
            console.log(error);
            this.errorMessage = error;
            this.isLoggedin = false;
            this.isLoginFailed = true;
        }
    );
}

}
