import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatSort } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthInterceptor } from './auth/AuthInterceptor';
import { CreateRecipientComponent } from './create-recipient/create-recipient.component';
import { HomeComponent } from './home/home.component';
import { ListRecipientComponent } from './list-recipient/list-recipient.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UpdateRecipientComponent } from './update-recipient/update-recipient.component';
import {MatIconModule} from '@angular/material/icon';
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    ListRecipientComponent,
    CreateRecipientComponent,
    UpdateRecipientComponent, 
  ],
  imports: [
    BrowserModule,
    MatTableModule,
    MatButtonModule, 
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule, 
    MatIconModule,
    HttpClientModule, BrowserAnimationsModule,
    
    
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: 
    AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }