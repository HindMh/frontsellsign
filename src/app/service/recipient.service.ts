import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Recipient } from "../model/recipient.model";

@Injectable({
    providedIn: 'root',
})
export class RecipientService {

  private readonly BASE_URL_API = environment.BASE_URL_API;

    private refreshRecipient = new BehaviorSubject<boolean>(false);
    flagRecipient$: Observable<boolean> = this.refreshRecipient.asObservable();

    private recipientBs = new BehaviorSubject<Recipient>(null);
    recipientNext$: Observable<Recipient> = this.recipientBs.asObservable();


    constructor(private http: HttpClient){}

    getAllRecipients(): Observable<any>{
        return this.http.get(this.BASE_URL_API+'list',
         {responseType:'json'})
                        .pipe(catchError(this.handleError));
    }
   
  removeOneRecipient(id:number): Observable<any>{
      return this.http.delete(this.BASE_URL_API+'delete/'+id,
       {responseType:'text'})
                      .pipe(catchError(this.handleError));
  }

createRecipient(recipient:Recipient): Observable<any>{
    return this.http.post(this.BASE_URL_API+'create/',recipient,
     {responseType:'json'})
                    .pipe(catchError(this.handleError));
}

updateRecipient(recipient:Recipient,id:number): Observable<any>{
  return this.http.put(this.BASE_URL_API+'update/'+id,recipient,
   {responseType:'json'})
                  .pipe(catchError(this.handleError));
}

searchRecipient(email:string): Observable<any>{
  return this.http.get(this.BASE_URL_API+`filter?email=${email}`,
   {responseType:'json'})
                  .pipe(catchError(this.handleError));
}

    private handleError(httpError: HttpErrorResponse) {
      let message:string = '';
      if (httpError.error instanceof ProgressEvent) {
        console.log('in progrss event')
        message = "Network error";
      }
      else {
        message = JSON.parse(httpError.error).message;
        console.error(           
          `Backend returned code ${httpError.status}, ` +
          `body was: ${httpError.error}`);
    }
    return throwError(
      'Something bad happened; please try again later. Error Message- ' + message);
    }

    public nextRefreshRecipient(flag) {
      this.refreshRecipient.next(flag);
    }

    public nextRecipientAction(recipient) {
      this.recipientBs.next(recipient);
    }
}