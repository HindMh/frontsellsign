import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, throwError } from "rxjs";
import { catchError, map } from 'rxjs/operators';
import { environment } from "src/environments/environment";
import { User } from "../model/user.model";


const headers = new HttpHeaders().set('Content-Type', 'application/json');
@Injectable({
  providedIn: 'root'
})
export class AuthService{
  
    private readonly BASE_URL_AUTH = environment.BASE_URL_AUTH;
    
    constructor(private http: HttpClient,private router:Router) {}

    signup(user: User): Observable<any>{
  
      return this.http.post(this.BASE_URL_AUTH + 'signup', user, { headers, responseType: 'text'})
                      .pipe(catchError(this.handleError));
    }

  
    login(user: string, password: string){
        return this.http.post<any>(this.BASE_URL_AUTH + 'signin', 
          {username: user, password:password}, {headers})
          .pipe(catchError(this.handleError),
            map(userData => {
              sessionStorage.setItem("username", user);
              let tokenStr = "Bearer " + userData.token;
              console.log("Token---  " + tokenStr);
              sessionStorage.setItem("token", tokenStr);
              sessionStorage.setItem("roles", JSON.stringify(userData.roles));
              return userData;
            })
          ); 
      }
    
  
  
     logout(){
        sessionStorage.clear()
        this.router.navigate(['/login']);
      }
    
      isLoggedIn(): boolean{
        return sessionStorage.getItem('username') !== null;
      }
    private handleError(httpError: HttpErrorResponse) {
      let message:string = '';
  
      if (httpError.error instanceof ProgressEvent) {
        message = "Network error";
      }
      else {
        message = httpError.error.message;
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong.
        console.error(
          `Backend returned code ${httpError.status}, ` +
          `body was: ${httpError.error}`);
      }
      // Return an observable with a user-facing error message.
      return throwError(message);
    }
  }