import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Recipient } from '../model/recipient.model';
import { RecipientService } from '../service/recipient.service';


@Component({
  selector: 'app-list-recipient',
  templateUrl: './list-recipient.component.html',
  styleUrls: ['./list-recipient.component.css']
})


export class ListRecipientComponent implements OnInit{
  
  datarecipient=new MatTableDataSource<Recipient>();
  recipientSubscription : Subscription;
  displayedColumnsRecipient :string[];
  displayedColumns: string[] = ['modification'];
  columns: string[] = [];
  searchFilter: string = '';

  @ViewChild(MatSort) sort: MatSort;

  constructor(private recipientService:RecipientService,private router:Router) { 
    
    this.displayedColumnsRecipient = ['id','civility',
    'firstname','lastname','email','adress','zipCode','city','cellPhone',
    'lastUpdatePlace','localisation','lastUpdateDate'];
  }

  ngOnInit(): void {

    this.getColumns().then((cols:string[])=>{
      this.displayedColumns.unshift(...cols);
      this.columns=cols;
    });
    this.recipientSubscription = this.recipientService.flagRecipient$
            .subscribe(flag => {
              this.refreshRecipientData(flag);
            })
    this.getRecipients();
  }

  ngAfterViewInit(): void {
    this.datarecipient.sort = this.sort;
  }

  refreshRecipientData(flag) {
    if(flag) {
      this.getRecipients();
    }
  }
  getRecipients(){
    
   
    
    this.recipientService.getAllRecipients().subscribe(
      data => {
        this.datarecipient.data=data;
      },error => {
       this.datarecipient.data = error;
      });
  }
  
  searchRecipientByFilter(){
    

  if(this.searchFilter !== null && this.searchFilter !== '' && this.searchFilter !== undefined) {
        this.recipientService.searchRecipient(this.searchFilter).subscribe(data=>{
          this.datarecipient.data=data
        })
  } else {
     this.getRecipients();
  }

  }
  delete(el:any){
    this.recipientService.removeOneRecipient(el).subscribe(data=>{
      this.getRecipients();
    });
  }

  update(recipientUpdate:Recipient){
    this.recipientService.nextRecipientAction(recipientUpdate);
    this.router.navigate(['/recipient/update'])
  }
  
  createRecipient(){
    this.router.navigate(['/recipient/create']);
  }
  
  getColumns(){
    /*assume this is an api*/
    return new Promise((resolve,reject)=>{
      resolve(this.displayedColumnsRecipient);
    })
  }

}
