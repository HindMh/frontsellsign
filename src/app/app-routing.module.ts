import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateRecipientComponent } from './create-recipient/create-recipient.component';
import { HomeComponent } from './home/home.component';
import { ListRecipientComponent } from './list-recipient/list-recipient.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UpdateRecipientComponent } from './update-recipient/update-recipient.component';




const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {path: 'signup', component: RegisterComponent}, 
    {path: 'login', component: LoginComponent}, 
    {path: 'home', component: HomeComponent},
    {path: 'recipients', component: ListRecipientComponent},
    {path: 'recipient/create', component: CreateRecipientComponent},
    {path: 'recipient/update', component: UpdateRecipientComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
